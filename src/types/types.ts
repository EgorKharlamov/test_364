export type Nullable<T> = T | null;

export type HTMLElementEvent<T extends HTMLElement> = Event & {
  target: T;
};
