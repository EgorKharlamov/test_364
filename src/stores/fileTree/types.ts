export interface TreeState {
  id: string;
  type: TreeNodeType;
  name: string;
  children?: TreeState[];
}

export enum TreeNodeType {
  File = "file",
  Directory = "directory",
}
