import type { TreeState } from "@/stores/fileTree/types";
import { TreeNodeType } from "@/stores/fileTree/types";
import { nanoid } from "nanoid";

export const initialTreeState: TreeState[] = [
  {
    id: nanoid(),
    type: TreeNodeType.Directory,
    name: "Dir 1",
    children: [
      {
        id: nanoid(),
        type: TreeNodeType.Directory,
        name: "Dir 1-1",
        children: [
          {
            id: nanoid(),
            type: TreeNodeType.File,
            name: "File 1-1-1",
          },
        ],
      },
      {
        id: nanoid(),
        type: TreeNodeType.File,
        name: "File 1-2",
      },
    ],
  },
  {
    id: nanoid(),
    type: TreeNodeType.Directory,
    name: "Dir 2",
    children: [
      {
        id: nanoid(),
        type: TreeNodeType.Directory,
        name: "Dir 2-1",
      },
      {
        id: nanoid(),
        type: TreeNodeType.File,
        name: "File 2-2",
      },
    ],
  },
  {
    id: nanoid(),
    type: TreeNodeType.File,
    name: "File 2",
  },
];
