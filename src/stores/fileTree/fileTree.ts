import { defineStore } from "pinia";
import { IdStores } from "@/stores/types";
import { initialTreeState } from "@/stores/fileTree/initialTreeState";
import { changeProps, removeObject, returnFound } from "find-and";
import { TreeNodeType } from "@/stores/fileTree/types";
import { nanoid } from "nanoid";
import type { Nullable } from "@/types/types";

export const useFileTreeStore = defineStore({
  id: IdStores.FileTreeStore,
  state: () => ({
    fileTree: initialTreeState,
  }),
  getters: {
    getFileTree: (state) => state.fileTree,
  },
  actions: {
    deleteNode(id: string) {
      this.fileTree = removeObject(this.fileTree, { id });
    },
    renameNode(id: string, name: string) {
      this.fileTree = changeProps(this.fileTree, { id }, { name });
    },
    addNode(parentId: string, name: string, type: Nullable<TreeNodeType>) {
      const object = returnFound(this.fileTree, { id: parentId });
      const newNode = {
        id: nanoid(),
        type: type ?? TreeNodeType.Directory,
        name,
      };
      if (object.children?.length) {
        object.children.push(newNode);
      } else {
        object.children = [newNode];
      }
    },
  },
});
