```bash
git clone https://gitlab.com/EgorKharlamov/test_364.git
cd test_364
```

run locally ([link](http://localhost:3000))
```bash
npm i && npm run dev
```

or with docker and docker-compose ([link](http://localhost))
```bash
docker-compose up -d
```
